"use strict";

const path = require('path');
const http = require('http');
const socketIO = require('socket.io');
const express = require('express');

const { generateMessage, getLocationLink } = require("./utils/message.js");
const { isRealString } = require("./utils/validation.js");
const Users = require("./utils/users.js");

const publicPath = path.join(__dirname,"/../public" );
const port = process.env.PORT || 3000;

console.log(`Public path : ${publicPath}`);

var app = express();
var server = http.createServer(app);
var io = socketIO(server);

const users = new Users();

app.use(express.static(publicPath));

// connection ön tanımlı bir olay.
io.on("connection", (socket) => {
  console.log("Yeni bir kullanıcı bağlandı.");

  socket.on("createEmail", (newEmail) => {
    console.log("Yeni bir email oluşturuldu", newEmail);
    socket.emit("newEmail", generateMessage("nadir@example.com","Merhaba socket.io"));
  });

  socket.on("join", (params, callback) => {

    if (!isRealString(params.name) || !isRealString(params.room)){
      callback("İsim ve oda bilgisi girmek zorunludur.");
      return;
    }

    if (users.userExist(params.name)){
      callback(`${params.name} ismi kullanımda lütfen başka bir isim seçiniz.`);
      return;
    }

    

    // herhangi bir gruba katılmak bu kadar basit
    socket.join(params.room);
    // socket.leave(params.room); // gruptan çık

    users.removeUser(socket.id); // varsa başka odadan çıkart bu kullanıcıyı.
    users.addUser(socket.id, params.name, params.room);

    io.to(params.room).emit("updateUserList", users.getUserList(params.room));

    // Belirli bir gruba mesaj göndermek için aşağıdaki metodları kullanabilirsin.
    // io.emit() -> io.to("roomName").emit()
    // socket.broadcast.emit() -> socket.broadcast().to("roomName").emit()

    socket.emit("newUser", generateMessage("Admin", "Welcome to the chat room"));
    socket.broadcast.to(params.room).emit("newUser", generateMessage("Admin",`${params.name} has joined.`));

    callback(); // hata olmasa da doğrulama fonksiyonunu çağır...
  });

  socket.on("createMessage", (createdMessage, dogrulama) => {

    // socket.emit sadece o socket ile bağlı olan kullanıcıya veri gönderir.
    // socket.emit("newMessage", generateMessage(createdMessage.from, createdMessage.text));

    // io.emit o anda bağlı tüm kullanıcılara veri gönderir.
    // io.emit("newMessage", generateMessage(createdMessage.from,createdMessage.text));

    // creatMessage olayını tetikleyen istemci dışında kalan tüm istemcilere gönder...
    // socket.broadcast.emit("newMessage", {});

    let user = users.getUser(socket.id);

    if (user && isRealString(createdMessage.text)){
      // İlgili odada mesajı gönderenin kendisi dahil herkese cevap gönder.
      io.to(user.room).emit("newMessage", generateMessage(user.name, createdMessage.text));
    }

    dogrulama();

  });

  socket.on("createLocationMessage", (coords, dogrulama) => {
    let user = users.getUser(socket.id);
    if (user) {
      // İlgili oda içinde mesajı gönderen kişi hariç herkese cevap gönder.
      socket.broadcast.to(user.room).emit("newMessage", generateMessage(user.name, getLocationLink(coords)));
    }
    dogrulama();
  });

  // Doğrulama amaçlı olarak istemciden gönderilen callback fonksiyonunu çalıştır.
  socket.on("dogrulamaliMesaj", (mesaj, callback) => {
    console.log("Doğrulamalı mesaj", mesaj);
    callback({
      status : "Success",
      action : "Hadi anlat bakalım"
    });
  });

  socket.on("disconnect", () => {
    console.log("Kullanıcı bağlantısı kesildi.");

    let user = users.removeUser(socket.id);

    if (user){
      io.to(user.room).emit("updateUserList", users.getUserList(user.room));
      io.to(user.room).emit("newMessage", generateMessage("Admin", `${user.name} has left the chat.`));
      socket.leave(user.room);
    }

  });

});

server.listen(port, () => {
  console.log("Express is waiting ruquests at port " + port);
});
