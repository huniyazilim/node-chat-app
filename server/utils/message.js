"use strict";

const moment = require('moment');

const generateMessage = (from, text) => {
  return {
    from,
    text,
    createdAt : moment().valueOf()
  }
}

const getLocationLink = function (coords) {
  const url = `https://www.google.com/maps?q=${coords.latitude},${coords.longitude}`;
  return `<a href="${url}" target="_blank">Here is my location</a>`;
}

module.exports = {
  generateMessage,
  getLocationLink
}
