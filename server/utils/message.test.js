"use strict";

const expect = require('expect');

const { generateMessage, getLocationLink } = require("./message.js");

describe("Generate Message", () => {
  it("Should generate a message", () => {
    let newMessage = generateMessage("Nadir", "Selam");
    expect(newMessage.createdAt).toBeA("number");
    expect(newMessage).toInclude({from : "Nadir", text:"Selam"});
  });
});

describe("Generate Location Message", () => {
  it ("Should generate a location message", () => {
    let newMessage = generateMessage("Nadir", getLocationLink({latitude:1, longitude:2}));
    expect(newMessage.createdAt).toBeA("number");
    expect(newMessage).toInclude({from : "Nadir"});
    expect(newMessage.text).toBe('<a href="https://www.google.com/maps?q=1,2" target="_blank">Here is my location</a>');
  });
});
