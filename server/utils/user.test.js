"use strict";

const expect = require('expect');
const Users = require("./users.js");

describe("Users class", () => {

  var myUsers = undefined;

  beforeEach( () => {
    myUsers = new Users();
    myUsers.users = [
      {
        id : "1",
        name : "Çınar",
        room : "Node Course"
      },
      {
        id : "2",
        name : "Olcay",
        room : "React Course"
      },
      {
        id : "3",
        name : "Nazan",
        room : "Node Course"
      },
    ]
  });

  it("should add a new user", () => {
    let users = new Users();
    let newUser = users.addUser("123","Nadir","Likom");
    expect(newUser).toExist();
    expect(users.users.length).toEqual(1);
    expect(users.users[0].id).toBe("123");
    expect(users.users[0].name).toBe("Nadir");
    expect(users.users[0].room).toBe("Likom");
    expect(users).toInclude({users : [{id:"123", name : "Nadir", room : "Likom"}]});
  });

  it("should get the users list", () => {
    let usersList = myUsers.getUserList("Node Course");
    expect(usersList).toBeAn("array");
    expect(usersList.length).toBe(2);
    expect(usersList).toInclude("Nazan");
    expect(usersList).toInclude("Çınar");
  });

  it("should remove an existing user", () => {
    let removedUser = myUsers.removeUser("1");
    expect(myUsers.users.length).toBe(2);
  });

  it("should not remove a non-existing user", () => {
    let removedUser = myUsers.removeUser("6");
    expect(myUsers.users.length).toBe(3);
  });

  it("should find an existing user", () => {
    let user = myUsers.getUser("1");
    expect(user).toExist();
    expect(user.id).toBe("1");
  });

  it("should not return a user when a non-existing user id is given", () => {
    let user = myUsers.getUser("6");
    expect(user).toNotExist();
  });

});
