"use strict";

const scrollToBottom = function () {
  let messageList = document.getElementById("message-list");
  let newMessage = messageList.lastChild;
  let lastMessage = messageList.childNodes[messageList.childNodes.length-1];

  let clientHeight = messageList.clientHeight;
  let scrollTop = messageList.scrollTop;
  let scrollHeight = messageList.scrollHeight;

  let newMessageHeight = newMessage.clientHeight;
  let lastMessageHeight = lastMessage ? lastMessage.clientHeight : 0;

  if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight){
    messageList.scrollTop = scrollHeight;
  }

}

const appendToList = function (msg) {
  const formattedTime = moment(msg.createdAt).format("h:mm a");
  const listElement = document.createElement("li");
  listElement.className = "message";
  let listHtml =  '<div class="message__title">'+
                    `<h4>${msg.from}</h4>`+
                    `<span> ${formattedTime}</span>`+
                  '</div>'+
                  '<div class="message__body">'+
                    `<p>${msg.text}</p>`
                  '</div>';
  listElement.innerHTML = listHtml;
  document.getElementById("message-list").append(listElement);
  scrollToBottom();
}

var socket = io();
// connect ön tanımlı bir olay.
socket.on("connect", function () {
  console.log("Sunucuya bağlanıldı.");
  const params = jQuery.deparam(window.location.search);
  console.log(params);
  socket.emit("join", params, function (err) {
    if (err){
      alert(err);
      window.location.href = "/"; // tekrar index/join sayfasına yönlendir.
    } else {
      console.log( "No error :)" );
    }
  });
});

// disconnect ön tanımlı bir olay.
socket.on("disconnect", function () {
  console.log("Sunucu bağlantısı kesildi.");
});

socket.on("updateUserList", function (users) {
  console.log(users);
  let ol = document.createElement("ol");
  users.forEach((user) => {
    let li = document.createElement("li");
    li.innerHTML = user;
    ol.appendChild(li);
  });
  let usersEl = document.getElementById("users");
  usersEl.innerHTML = "";
  usersEl.appendChild(ol);
});

// custom event
// Sunucu tarafından gelecek "newEmail" adlı olayı dinle...
socket.on("newEmail", function (email) {
  console.log("Yeni email geldi.", email);
});

socket.on("newMessage", function (newMessage) {
  appendToList(newMessage);
});

socket.on("newUser", function (msg) {
  appendToList(msg);
});

var yeniMailGonder = function () {
  socket.emit("createEmail", {
    to : "cinar@example.com",
    text : "Seni seviyorum Çınar."
  });
}

var yeniMesajGonder = function () {
  socket.emit("createMessage", {
    from : "cinar@example.com",
    text : "Seni seviyorum baba."
  });
}

// Bu olay sunucuya ulaştığında doğrulama amaçlı olarak ikinci parametre olarak
// verilen fonksiyon çalıştırılıyor ve opsiyonel olarak bu fonksiyona sunucu
// tarafından veri de gönderilebilir.
var dogrulamaliMesajGonder = function () {
  socket.emit("dogrulamaliMesaj", {
    from : "Nadir",
    text : "Hadi doğrula bakalım"
  }, function (data) { // doğrulama olarak bu fonk. çalıştırılacak.
    console.log("Sunucu bu mesajı karşıladı.", data);
  });
}
